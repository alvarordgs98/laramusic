<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>{{$titulo or 'Curso de Laravel'}}</title>

        @stack('scripts')
    </head>
    <body>
        <div>Topo 2</div>
            @yield('content')
        <div>footer</div>

        @stack('scripts-footer')
    </body>
</html>