<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8"/>
    <title>{{$titulo or 'Painel Curso'}}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- CSS -->
    <link rel="stylesheet" href="/assets/painel/css/style.css">

</head>
<body>

    <div class="container">
        @yield('content')
    </div>

    <!-- jQuery-->
    <script src="{{url('/assets/js/jquery.min.js')}}"></script>

    <!-- Bootstrap JS -->
    <script src="{{url('/assets/js/bootstrap.min.js')}}"></script>

</body>
</html>