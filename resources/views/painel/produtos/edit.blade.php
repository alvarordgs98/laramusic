@extends('painel.template.template1')

@section('content')

    <div class="top-header">

        <h1>Editar produto</h1>

        <a href="{{url('/produto')}}">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
            </svg>
        </a>
    </div>

    @if(isset($errors) && count($errors) > 0)
        <div class="alert-danger alert">
            @foreach($errors -> all() as $error)
                {{$error}} <br>
            @endforeach
        </div>
    @endif

    <form method="post" action="/produto/{{$produto->id}}" class="form">
        <input type="hidden" name="_method" value="PUT">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" name="nome" placeholder="Insira o nome do produto" class="form-control" value="{{$produto->nome}}">
        </div>
        <div class="form-group">
            <input type="text"  name="cod" placeholder="Insira o codigo do produto" class="form-control" value="{{$produto->cod}}">
        </div>
        <div class="form-group">
            <input type="submit" name="enviar" value="Enviar" class="btn btn-success">
        </div>
    </form>
@endsection