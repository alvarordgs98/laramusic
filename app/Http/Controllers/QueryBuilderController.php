<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class QueryBuilderController extends Controller
{
    public function tests()
    {
        //Select
        //$users = DB::table('users') -> get();
        //dd(DB::table('users') -> get());

        //Insert
        /*$insere = DB::table('users') -> insert ([
           [
               'name' => 'Fulano',
               'email' => 'fulano@site.com',
               'password' => bcrypt('0123')
           ],
           [
              'name' => 'Ciclano',
              'email' => 'ciclano@site.com',
              'password' => bcrypt('3210')
           ] ,
        ]);
        dd($insere);*/

        //Update
        /*$update = DB::table('users') -> where('email', 'ciclano@site.com') -> update ([
            'name' => 'Name update',
            'email' => 'email-update@site.com'
        ]);
        dd($update);*/
    }

    public function tests2()
    {
        //return DB::table('users') -> get();
        //return DB::table('users') -> select('id as id_user', 'name as nome', 'email') -> get();
        //return DB::table('produtos') -> pluck('nome');
        //dd(DB::table('users') -> first());
    }

    public function tests3()
    {
        //return DB::table('produtos') -> count();
        //return DB::table('produtos') -> max('cod');
        //return DB::table('produtos') -> min('cod');
        //return DB::table('produtos') -> avg('cod');
    }

    public function where()
    {
        //$produtos = DB::table('produtos') -> select('id', 'nome') -> where('id', '<>', '2') -> get();
        //$produtos = DB::table('produtos') -> where('id', '1') -> orWhere('id', '<>', '2') -> get();
        //$produtos = DB::table('produtos') -> whereIn('id', [ 1, 2]) -> get();
        //$produtos = DB::table('produtos') -> whereNull('created_at')-> get();
        //$produtos = DB::table('produtos') -> whereBetween('cod', [500, 10000]) -> get();
        //return $produtos;
    }

    public function tests4()
    {
        //$users = DB::table('users') -> select('id', 'name') -> orderBy('name', 'ASC') -> get();
        //$users = DB::table('users') -> select('id', 'name') -> skip('0') -> take('6') -> get();
        //return $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
