<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\painel\Produto;
use Illuminate\Support\Facades\Validator;

class ProdutoController extends Controller
{

    protected $produto, $request;

    public function __construct(Produto $produto, Request $request)
    {
        $this->produto = $produto;
        $this->request = $request;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //Recupera os produtos cadastrados...
        $produtos = $this->produto->paginate(4);

        //Mostra a view com a listagem dos produtos
        return view('painel.produtos.index', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('painel.produtos.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request -> all());
        //dd($request -> input('nome'));
        //dd($request -> only(['nome', 'email']));
        //dd($request -> except('email'));

        $dadosForm = $this->request->all();

        $valid = Validator::make($dadosForm, Produto::$rules);

        if($valid -> fails()){
            return redirect('/produto/create')
                        ->withErrors($valid)
                        ->withInput();
        }

        $insert = $this->produto->create($dadosForm);

        if ( $insert ){
            return redirect('/produto');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Recupera o produto pelo seu id
        $produto = $this->produto->find($id);

        //Mostra a view com os dados do produto
        return view('painel.produtos.show', compact('produto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Recupera o produto pelo seu id
        $produto = $this->produto->find($id);

        //Mostra a view editar os dados
        return view('painel.produtos.create-edit', compact('produto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $dadosForm = $this->request->all();

        //Regras especiais para o editar
        $rules = [
            'nome' => 'required |min:3 |max:150',
            'cod'  => "required | numeric|unique:produtos,cod,{$id}"
        ];

        $valid = Validator::make($dadosForm, $rules);

        if($valid -> fails()){
            return redirect("/produto/{$id}/edit")
                ->withErrors($valid)
                ->withInput();
        }

        //Recuperar o produto
        $produto = $this->produto->find($id);

        //Editar o produto
        $update = $produto->update($this->request->all());

        if( $update ) {
            return redirect('/produto');
        }

        return redirect("/produto/{$id}/edit")
                    ->withErrors('errors', 'Falha ao editar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Cria um objeto de produto através do id
        $prod = $this->produto->find($id);

        $delete = $prod->delete();

        if( $delete ){
            return redirect('/produto');
        }else {
            return redirect("/produto/$produto->id");
        }
    }
}
