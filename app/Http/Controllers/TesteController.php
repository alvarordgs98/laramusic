<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TesteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $varTeste = 123;

        return view('view') -> with('varTeste',$varTeste);
    }

    public function edit($id)
    {
        return "Editar {$id}";
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'TesteController@create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function matriculas()
    {
        $titulo = 'Matriculas';
        $mes = date('m');
        $mes2 = date('M');
        $min = date('i');
        $testeXss = '<script>alert("Hello world");</script>';

        return view('painel00.matriculas.index', compact('titulo', 'testeXss', 'mes', 'mes2', 'min'));
    }

    public function financeiro()
    {
        $titulo = 'Financeiro';
        return view('painel00.financeiro.index', compact('titulo'));
    }
}
