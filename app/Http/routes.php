<?php

Route::resource('produto', 'Painel\ProdutoController');

//---------------------------------------------------------------

Route::get('models', function(){
    $produtos = new App\Models\painel\Produto;

    return $produtos -> get();
});

Route::get('teste-models', 'TesteModelsController@index');

//-----------------------------------------------------------------

Route::get('/', function() {
    return view('welcome');
});

//-----------------------------------------------------------------

Route::get('users', function(){
   return DB::table('users') -> get();
});

//-----------------------------------------------------------------

Route::get('query-builder', 'QueryBuilderController@tests');

Route::get('query-builder2', 'QueryBuilderController@tests2');

Route::get('query-builder3', 'QueryBuilderController@tests3');

Route::get('where', 'QueryBuilderController@where');

Route::get('query-builder4', 'QueryBuilderController@tests4');













/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/painel00/matriculas', 'TesteController@matriculas');
Route::get('/painel00/financeiro', 'TesteController@financeiro');

Route::resource('resource', 'RestFullResourceController');

Route::get('/create', [
    'middleware' => 'auth',
    'uses' => 'TesteController@create'
]);

Route::get('/edit/{id}', 'TesteController@edit');

Route::group(['middleware' => ['horario-acesso', 'aniversario']], function() {
    Route::get('/promocoes/aniversario', 'PromocoesController@aniversario');
});

Route::get('/', 'TesteController@index');*/

