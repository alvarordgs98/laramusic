<?php

namespace App\Http\Middleware;

use Closure;

class HorarioAcesso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $horaAtual = date('Hm');

        if($horaAtual >= '0800' && $horaAtual <= '1800'){
            return $next($request);
        } else {
            return redirect('/');
        }

    }
}
