<?php

namespace App\Models\painel;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['nome', 'cod'];
    //protected $guarded = ['tipo'];

    //Regras para interagir dados
    static $rules = [
        'nome' => 'required |min:3 |max:150',
        'cod'  => 'required | numeric|unique:produtos'
    ];
}
